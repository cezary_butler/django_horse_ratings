from django.db.models import query, Avg
from rest_framework import viewsets, permissions
from .models import Horse, Rating
from .serializers import HorseSerializer, RatingSerializer

class HorseViewSet(viewsets.ModelViewSet):
    queryset = Horse.objects.all()
    permissions = [permissions.AllowAny]
    serializer_class = HorseSerializer
    
    def get_queryset(self):
        return Horse.objects.annotate(
            avg_rating=Avg('ratings__value'),
        )

class RatingViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all()
    permissions = [permissions.AllowAny]
    serializer_class = RatingSerializer
