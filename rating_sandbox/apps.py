from django.apps import AppConfig


class RatingSandboxConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rating_sandbox'
