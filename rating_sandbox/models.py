from django.db import models

class Horse(models.Model):
    name = models.CharField(max_length=100)
    birthday = models.DateField(null=False)

   
class Rating(models.Model):
    value = models.IntegerField()
    horse = models.ForeignKey(Horse, related_name='ratings', on_delete=models.DO_NOTHING)
