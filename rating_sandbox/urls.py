from rest_framework import routers, urlpatterns
from django.urls import path

from .api import HorseViewSet, RatingViewSet

router = routers.DefaultRouter()
router.register('api/horses', HorseViewSet, 'horses')
router.register('api/ratings', RatingViewSet, 'ratigs')

urlpatterns = router.urls
