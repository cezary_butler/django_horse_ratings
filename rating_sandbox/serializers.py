from rest_framework import serializers
from .models import Horse, Rating

class HorseSerializer(serializers.ModelSerializer):
    avg_rating = serializers.IntegerField(read_only=True)
    class Meta:
        model = Horse
        fields = '__all__'

class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = '__all__' #todo limit fields
    
